package app.example.mx.carros;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class AgregarCarro extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long carroId;
    private EditText carroEditText;
    private EditText descripcionEditText;
    private EditText marcaEditText;
    private EditText modeloEditText;
    private EditText serieEditText;
    private EditText placasEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_carro);

        carroEditText = (EditText) findViewById(R.id.nombre_edit_text);
        descripcionEditText = (EditText) findViewById(R.id.description_edit_text);
        marcaEditText = (EditText) findViewById(R.id.marca_edit_text);
        modeloEditText = (EditText) findViewById(R.id.modelo_edit_text);
        serieEditText = (EditText) findViewById(R.id.serie_edit_text);
        placasEditText = (EditText) findViewById(R.id.placas_edit_text);

        carroId = getIntent().getLongExtra(DetalleCarro.EXTRA_Carro_ID, -1L);

        if (carroId != -1L){
            getSupportLoaderManager().initLoader(0, null, this);
        }

        findViewById(R.id.boton_agregar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        String nombreCarro = carroEditText.getText().toString();
        String descripcionCarro = descripcionEditText.getText().toString();
        String marcaCarro = marcaEditText.getText().toString();
        String modeloCarro = modeloEditText.getText().toString();
        String serieCarro = serieEditText.getText().toString();
        String placasCarro = placasEditText.getText().toString();

        new CreateCarroTask(this, nombreCarro, descripcionCarro, marcaCarro, modeloCarro, serieCarro, placasCarro, carroId).execute();

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CarroLoader(this,carroId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_NAME);
            String nombreCarro= data.getString(nameIndex);

            int descripcionIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_DESCRIPTION);
            String descripcionCarro= data.getString(descripcionIndex);

            int marcaIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_MARCA);
            String marcaCarro= data.getString(marcaIndex);

            int modeloIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_MODELO);
            String modeloCarro= data.getString(modeloIndex);

            int serieIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_SERIE);
            String serieCarro= data.getString(serieIndex);

            int placasIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_PLACAS);
            String placasCarro= data.getString(placasIndex);

            carroEditText.setText(nombreCarro);
            descripcionEditText.setText(descripcionCarro);

            marcaEditText.setText(marcaCarro);
            modeloEditText.setText(modeloCarro);

            serieEditText.setText(serieCarro);
            placasEditText.setText(placasCarro);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class CreateCarroTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String carroName;
        private String carroDesc;
        private String carroMarc;
        private String carroMode;
        private String carroSeri;
        private String carroPlac;

        private long carroId;

        public CreateCarroTask(Activity activity, String name, String desc, String marc, String mode, String seri, String plac, long carroId){
            weakActivity = new WeakReference<Activity>(activity);
            carroName = name;
            carroDesc = desc;
            carroMarc = marc;
            carroMode = mode;
            carroSeri = seri;
            carroPlac = plac;

            this.carroId = carroId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            Boolean success = false;

            if (carroId != -1L) {
                int filasAfectadas = CarrosDatabase.actualizaCarro(appContext,carroName,carroDesc,carroMarc, carroMode, carroSeri,carroPlac,carroId);
                success = (filasAfectadas !=0);
            } else {
                long id = CarrosDatabase.insertaCarro(appContext, carroName, carroDesc, carroMarc, carroMode, carroSeri, carroPlac);
                success = (id != -1L);
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Grabación Correcta", Toast.LENGTH_SHORT).show();
            }
                else
                    {
                        Toast.makeText(context, "No se guardo", Toast.LENGTH_SHORT).show();
                    }
            context.finish();
        }
    }



}
