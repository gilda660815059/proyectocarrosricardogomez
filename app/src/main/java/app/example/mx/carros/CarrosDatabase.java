package app.example.mx.carros;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.annotation.StringDef;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Admon7 on 29/10/16.
 */

public class CarrosDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "carros.db";
    private static final String TABLE_NAME = "Carros";

    public static final String COL_NAME = "nombre";
    public static final String COL_DESCRIPTION = "descripcion";
    public static final String COL_MARCA = "marca";
    public static final String COL_MODELO = "modelo";
    public static final String COL_SERIE = "serie";
    public static final String COL_PLACAS = "placas";


    public CarrosDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createQuery =
                "CREATE TABLE " + TABLE_NAME +
                        " (_id INTEGER PRIMARY KEY, "
                        + COL_NAME + " TEXT NOT NULL COLLATE UNICODE, "
                        + COL_DESCRIPTION + " TEXT NOT NULL, "
                        + COL_MARCA + " TEXT NOT NULL, "
                        + COL_MODELO + " TEXT NOT NULL, "
                        + COL_SERIE + " TEXT NOT NULL, "
                        + COL_PLACAS + " TEXT NOT NULL) " ;

    db.execSQL(createQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String upgradeQuery = "DROP TABLE IF EXIST " + TABLE_NAME;
        db.execSQL(upgradeQuery);

    }

    public static long insertaCarro (Context context, String nombre, String descripcion, String marca, String modelo, String serie, String placas){

        SQLiteOpenHelper dbOpenHelper = new CarrosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorCarro = new ContentValues();
        valorCarro.put(COL_NAME,nombre);
        valorCarro.put(COL_DESCRIPTION,descripcion);
        valorCarro.put(COL_MARCA,marca);
        valorCarro.put(COL_MODELO,modelo);
        valorCarro.put(COL_SERIE,serie);
        valorCarro.put(COL_PLACAS,placas);

        long result = -1L;
        try {
            result = database.insert(TABLE_NAME, null, valorCarro);

            if (result != -1L)
                {
                    LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                    Intent intentFilter = new Intent(CarrosLoader.ACTION_RELOAD_TABLE);
                    broadcastManager.sendBroadcast(intentFilter);
                }
            } finally  {
                dbOpenHelper.close();
            }


        return result;

    }

    public static Cursor devuelveTodos(Context context){

        SQLiteOpenHelper dbOpenHelper = new CarrosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[]{COL_NAME, COL_DESCRIPTION, COL_MARCA, COL_MODELO, COL_SERIE, COL_PLACAS, BaseColumns._ID},
                null, null, null, null,
                COL_NAME+ " ASC");

    }

    public static Cursor devuelveConId(Context context, long identificador){

        SQLiteOpenHelper dbOpenHelper = new CarrosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                new String[]{COL_NAME, COL_DESCRIPTION, COL_MARCA, COL_MODELO, COL_SERIE, COL_PLACAS, BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null, null,
                COL_NAME+ " ASC");
    }


    public static int eliminaConId(Context context, long CarroId) {

        SQLiteOpenHelper dbOpenHelper = new CarrosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME, BaseColumns._ID + "=?",
                new String[] {String.valueOf(CarroId)});

        if (resultado != 0)
        {
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(CarrosLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();
        return resultado;
    }


    public static int actualizaCarro (Context context, String nombre,  String descripcion, String marca, String modelo, String serie, String placas, long CarroId){

        SQLiteOpenHelper dbOpenHelper = new CarrosDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorCarro = new ContentValues();
        valorCarro.put(COL_NAME,nombre);
        valorCarro.put(COL_DESCRIPTION,descripcion);
        valorCarro.put(COL_MARCA,marca);
        valorCarro.put(COL_MODELO,modelo);
        valorCarro.put(COL_SERIE,serie);
        valorCarro.put(COL_PLACAS,placas);

        int result = database.update(
                TABLE_NAME,
                valorCarro,
                BaseColumns._ID + " =?",
                new String[]{String.valueOf(CarroId)});

            if (result != 0){

                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(CarrosLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

            dbOpenHelper.close();

        return result;

}
}
