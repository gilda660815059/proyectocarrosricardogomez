package app.example.mx.carros;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SearchViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetalleCarro extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private TextView nombreTextView;
    private TextView descripcionTextView;
    private TextView marcaTextView;
    private TextView modeloTextView;
    private TextView serieTextView;
    private TextView placasTextView;

    private long CarroId;
    public static final String EXTRA_Carro_ID = "Carro.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_carro);

        nombreTextView = (TextView) findViewById(R.id.nombre_text_view);
        descripcionTextView = (TextView) findViewById(R.id.descripcion_text_view);
        marcaTextView = (TextView) findViewById(R.id.marca_text_view);
        modeloTextView = (TextView) findViewById(R.id.modelo_text_view);
        serieTextView = (TextView) findViewById(R.id.serie_text_view);
        placasTextView = (TextView) findViewById(R.id.placas_text_view);

        Intent intencion = getIntent();
        CarroId = intencion.getLongExtra(MainActivity.EXTRA_ID_CARRO, -1L);

        getSupportLoaderManager().initLoader(0, null, this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new DeleteCarroTask(DetalleCarro.this,CarroId).execute();

                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent =new Intent(DetalleCarro.this, AgregarCarro.class);
                        intent.putExtra(EXTRA_Carro_ID, CarroId);
                        startActivity(intent);


                    }
                }
        );







    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CarroLoader(this,CarroId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_NAME);
            String nombreCarro= data.getString(nameIndex);

            int descripcionIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_DESCRIPTION);
            String descripcionCarro= data.getString(descripcionIndex);

            int marcaIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_MARCA);
            String marcaCarro= data.getString(marcaIndex);

            int modeloIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_MODELO);
            String modeloCarro= data.getString(modeloIndex);

            int serieIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_SERIE);
            String serieCarro= data.getString(serieIndex);

            int placasIndex = data.getColumnIndexOrThrow(CarrosDatabase.COL_PLACAS);
            String placasCarro= data.getString(placasIndex);


            nombreTextView.setText(nombreCarro);
            descripcionTextView.setText(descripcionCarro);
            marcaTextView.setText(marcaCarro);
            modeloTextView.setText(modeloCarro);
            serieTextView.setText(serieCarro);
            placasTextView.setText(placasCarro);




        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }



    public static class DeleteCarroTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private long CarroId;

        public DeleteCarroTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            CarroId = id;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasAfectadas = CarrosDatabase.eliminaConId(appContext, CarroId);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Eliminación Correcta", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "No se guardo", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }






}
