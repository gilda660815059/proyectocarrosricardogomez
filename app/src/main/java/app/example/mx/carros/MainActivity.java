package app.example.mx.carros;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import static app.example.mx.carros.CarrosDatabase.COL_NAME;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CarrosLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        adaptador.swapCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        adaptador.swapCursor(null);

    }


    public final static String EXTRA_ID_CARRO = "carros.ID_CARRO";

    private SimpleCursorAdapter adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listaDecarros = (ListView) findViewById(R.id.activity_main);
        listaDecarros.setOnItemClickListener(this);

        adaptador = new SimpleCursorAdapter(
                        this,
                        android.R.layout.simple_list_item_1,
                        null,
                        new String[]{CarrosDatabase.COL_NAME},
                        new int[]{android.R.id.text1},
                        0);

            listaDecarros.setAdapter(adaptador);

            getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.boton_nuevo_carro).setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Intent intento = new Intent(MainActivity.this, AgregarCarro.class);
                startActivity(intento);

    }
    });

}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



        Intent intencion = new Intent(this,DetalleCarro.class);

        intencion.putExtra(EXTRA_ID_CARRO, adaptador.getItemId(position));

        startActivity(intencion);

    }
}
